﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace convert_mp4_to_yt_gif_app
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            textBox3.Text = !string.IsNullOrEmpty(Environment.UserName) ? Environment.UserName : "";
        }

        public void button1_Click(object sender, EventArgs e)
        {
            var fileContent = string.Empty;
            var filePath = string.Empty;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "mp4 files (*.mp4)|*.mp4|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;
                    label3.Text = filePath.Length > 60 ? "..." + filePath.Substring(filePath.Length - 45) : filePath;
                    label4.Text = filePath;
                    label9.Visible = false;

                    //Read the contents of the file into a stream
                    var fileStream = openFileDialog.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        fileContent = reader.ReadToEnd();
                    }
                }
            }
        }

        private async void button2_Click(object sender, EventArgs e)
        {
            string title = textBox1.Text;
            string desc = textBox2.Text;
            string videoFile = label4.Text;
            string initials = textBox3.Text;

            if (!string.IsNullOrEmpty(title) && !string.IsNullOrEmpty(desc) && !string.IsNullOrEmpty(label4.Text) && !string.IsNullOrEmpty(initials))
            {
                await Program.runFunctions(initials, title, desc, videoFile);
                textBox1.Text = "";
                textBox2.Text = "";
                label3.Text = "no file selected";
                label4.Text = "";
                label6.Visible = false;
                label7.Visible = false;
                label8.Visible = false;
                label9.Visible = false;
                MessageBox.Show("The link and thumbnail has been pasted to your clipboard!");
            }
            else
            {
                if (string.IsNullOrEmpty(initials))
                {
                    label6.Visible = true;
                }
                else
                {
                    label6.Visible = false;
                }
                if (string.IsNullOrEmpty(title))
                {
                    label7.Visible = true;
                }
                else
                {
                    label7.Visible = false;
                }
                if (string.IsNullOrEmpty(desc))
                {
                    label8.Visible = true;
                }
                else
                {
                    label8.Visible = false;
                }
                if (string.IsNullOrEmpty(label4.Text))
                {
                    label9.Visible = true;
                }
                else
                {
                    label9.Visible = false;
                }
            }
        }

    }
}
