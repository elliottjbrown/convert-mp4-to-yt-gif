using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using MediaToolkit.Model;
using MediaToolkit.Options;
using MediaToolkit;
using Google.Apis.YouTube.v3.Data;
using Google.Apis.Upload;
using Google.Apis.Auth.OAuth2;
using Google.Apis.YouTube.v3;
using System.Threading;
using Google.Apis.Services;
using System.Reflection;
using System.Diagnostics;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Net;
using AnimatedGif;
using System.Drawing.Drawing2D;
using System.Drawing.Text;

namespace convert_mp4_to_yt_gif_app
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

        }

        public static async Task runFunctions(string initials, string title, string desc, string inputPath)
        {

            // input field
            string dir = Path.GetDirectoryName(inputPath);

            // this doesn't work, something to do with google quota - I presume CL Tools already has this working. extract videoId from response.
            //Video ytVideo = await UploadVideoAsync(title, desc, inputPath);
            string videoId = "b6OvrRbGU68";

            double from = 100; // input field
            double duration = 5; // input field
            string outputPath = dir + "\\shortened_" + Path.GetFileName(inputPath);

            await ShortenVideo(inputPath, outputPath, from, duration);

            string sourcePath = outputPath;
            string convertedFilePath = Path.GetFileNameWithoutExtension(inputPath);
            string outputGifPath = dir + "\\" + convertedFilePath + ".gif";

            await CreateGifFromMp4(sourcePath, outputGifPath);
            await OverlayImageOnGif(outputGifPath);

            //copy gif to clipboard            
            string forClipBoard = ClipBoardTemplate("templates/clipBoardTemplate.txt", outputGifPath, GetVideoUrl(initials, videoId), title);
            CopyToClipboard(forClipBoard, title + "\n" + desc);
            await Task.Yield();
        }

        public static async Task OverlayImageOnGif(string outputPath)
        {
            const int IMG_QUALITY = 100;
            // background GIF
            byte[] bgGif_ms = File.ReadAllBytes(outputPath);

            Image[] bgGif_frames = getFrames(bgGif_ms);

            // overlayImage 
            byte[] imageBytes = File.ReadAllBytes("images/button-play.png");
            Image overlayImage = ByteArrayToImage(imageBytes);
            

            List<byte[]> newFrames = new List<byte[]>();
            if (bgGif_frames.Length != 0)
            {
                for (var i = 0; i < bgGif_frames.Length; i++)
                {
                    i+=4;
                    //i++;
                    int w = bgGif_frames[i].Width;
                    int h = bgGif_frames[i].Height;
                    int desiredWidth = 320;
                    int desiredHeight = (int)(h * ((double)desiredWidth / w));

                    Image resized_image = ResizeImage(bgGif_frames[i], new Size(desiredWidth, desiredHeight));
                    int x = (resized_image.Width - overlayImage.Width) / 2;
                    int y = (resized_image.Height - overlayImage.Height) / 2;
                    newFrames.Add(await OverlayImage(resized_image, overlayImage, x, y));

                }

            }

            byte[] array = CreateGif(newFrames);
            File.WriteAllBytes(outputPath, array);
            
        }

        public static async Task<byte[]> OverlayImage(Image backgroundImage, Image overlayImage, int x, int y)
        {

            byte[] bitmapData = null;
            try
            {
                    Graphics canvas = Graphics.FromImage(backgroundImage);

                    // ------------------------------------------
                    // Ensure the best possible quality rendering
                    // ------------------------------------------
                    // The smoothing mode specifies whether lines, curves, and the edges of filled areas use smoothing (also called antialiasing). 
                    // One exception is that path gradient brushes do not obey the smoothing mode. 
                    // Areas filled using a PathGradientBrush are rendered the same way (aliased) regardless of the SmoothingMode property.
                    canvas.SmoothingMode = SmoothingMode.AntiAlias;
                    canvas.InterpolationMode = InterpolationMode.HighQualityBicubic; // The interpolation mode determines how intermediate values between two endpoints are calculated.
                    canvas.PixelOffsetMode = PixelOffsetMode.HighQuality; // Use this property to specify either higher quality, slower rendering, or lower quality, faster rendering of the contents of this Graphics object.
                    canvas.TextRenderingHint = TextRenderingHint.AntiAliasGridFit; // This one is important

                // overlayImage
                canvas.DrawImage(overlayImage, x, y);

                // Flush all graphics changes to the bitmap
                canvas.Flush();
                

                //Convert to byte array
                MemoryStream memoryStream = new MemoryStream();

                using (memoryStream)
                {
                    backgroundImage.Save(memoryStream, ImageFormat.Png);
                    bitmapData = memoryStream.ToArray();
                }

            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            await Task.Yield();
            return bitmapData;
        }

        public static byte[] CreateGif(List<byte[]> images)
        {
            var imageStream = new MemoryStream();
            //using (var gif = AnimatedGif.AnimatedGif.Create("test.gif", 33))
            using (var gif = new AnimatedGif.AnimatedGifCreator(imageStream, 132))
            {
                foreach (byte[] item in images)
                {
                    using (var ms = new MemoryStream(item))
                    {
                        var img = Image.FromStream(ms);
                        gif.AddFrame(img, delay: -1, quality: GifQuality.Bit8);
                        //gif.AddFrame(img, delay: -1, quality: GifQuality.Bit4);
                    }
                }

                byte[] bitmapData = imageStream.ToArray();

                return bitmapData;

            }
        }

        public static void CopyToClipboard(string html, string plainText)
        {
            Clipboard.Clear();
            var dataObject = ClipboardHelper.CreateDataObject(html, plainText);
            Clipboard.SetDataObject(dataObject);
        }

        /// <summary>
        /// Helper to encode and set HTML fragment to clipboard.<br/>
        /// See http://theartofdev.com/2014/06/12/setting-htmltext-to-clipboard-revisited/.<br/>
        /// <seealso cref="CreateDataObject"/>.
        /// </summary>
        /// <remarks>
        /// The MIT License (MIT) Copyright (c) 2014 Arthur Teplitzki.
        /// </remarks>
        public static class ClipboardHelper
        {
            #region Fields and Consts

            /// <summary>
            /// The string contains index references to other spots in the string, so we need placeholders so we can compute the offsets. <br/>
            /// The <![CDATA[<<<<<<<]]>_ strings are just placeholders. We'll back-patch them actual values afterwards. <br/>
            /// The string layout (<![CDATA[<<<]]>) also ensures that it can't appear in the body of the html because the <![CDATA[<]]> <br/>
            /// character must be escaped. <br/>
            /// </summary>
            private const string Header = @"Version:0.9
StartHTML:<<<<<<<<1
EndHTML:<<<<<<<<2
StartFragment:<<<<<<<<3
EndFragment:<<<<<<<<4
StartSelection:<<<<<<<<3
EndSelection:<<<<<<<<4";

            /// <summary>
            /// html comment to point the beginning of html fragment
            /// </summary>
            public const string StartFragment = "<!--StartFragment-->";

            /// <summary>
            /// html comment to point the end of html fragment
            /// </summary>
            public const string EndFragment = @"<!--EndFragment-->";

            /// <summary>
            /// Used to calculate characters byte count in UTF-8
            /// </summary>
            private static readonly char[] _byteCount = new char[1];

            #endregion

            /// <summary>
            /// Create <see cref="DataObject"/> with given html and plain-text ready to be used for clipboard or drag and drop.<br/>
            /// Handle missing <![CDATA[<html>]]> tags, specified start\end segments and Unicode characters.
            /// </summary>
            /// <remarks>
            /// <para>
            /// Windows Clipboard works with UTF-8 Unicode encoding while .NET strings use with UTF-16 so for clipboard to correctly
            /// decode Unicode string added to it from .NET we needs to be re-encoded it using UTF-8 encoding.
            /// </para>
            /// <para>
            /// Builds the CF_HTML header correctly for all possible HTMLs<br/>
            /// If given html contains start/end fragments then it will use them in the header:
            /// <code><![CDATA[<html><body><!--StartFragment-->hello <b>world</b><!--EndFragment--></body></html>]]></code>
            /// If given html contains html/body tags then it will inject start/end fragments to exclude html/body tags:
            /// <code><![CDATA[<html><body>hello <b>world</b></body></html>]]></code>
            /// If given html doesn't contain html/body tags then it will inject the tags and start/end fragments properly:
            /// <code><![CDATA[hello <b>world</b>]]></code>
            /// In all cases creating a proper CF_HTML header:<br/>
            /// <code>
            /// <![CDATA[
            /// Version:1.0
            /// StartHTML:000000177
            /// EndHTML:000000329
            /// StartFragment:000000277
            /// EndFragment:000000295
            /// StartSelection:000000277
            /// EndSelection:000000277
            /// <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
            /// <html><body><!--StartFragment-->hello <b>world</b><!--EndFragment--></body></html>
            /// ]]>
            /// </code>
            /// See format specification here: http://msdn.microsoft.com/library/default.asp?url=/workshop/networking/clipboard/htmlclipboard.asp
            /// </para>
            /// </remarks>
            /// <param name="html">a html fragment</param>
            /// <param name="plainText">the plain text</param>
            public static DataObject CreateDataObject(string html, string plainText = "")
            {
                html = html ?? String.Empty;
                var htmlFragment = GetHtmlDataString(html);

                // re-encode the string so it will work correctly (fixed in CLR 4.0)
                if (Environment.Version.Major < 4 && html.Length != Encoding.UTF8.GetByteCount(html))
                    htmlFragment = Encoding.Default.GetString(Encoding.UTF8.GetBytes(htmlFragment));

                var dataObject = new DataObject();
                dataObject.SetData(DataFormats.Html, htmlFragment);
                dataObject.SetData(DataFormats.Text, plainText);
                //dataObject.SetData(DataFormats.UnicodeText, plainText);
                return dataObject;
            }

            /// <summary>
            /// Clears clipboard and sets the given HTML and plain text fragment to the clipboard, providing additional meta-information for HTML.<br/>
            /// See <see cref="CreateDataObject"/> for HTML fragment details.<br/>
            /// </summary>
            /// <example>
            /// ClipboardHelper.CopyToClipboard("Hello <b>World</b>", "Hello World");
            /// </example>
            /// <param name="html">a html fragment</param>
            /// <param name="plainText">the plain text</param>
            public static void CopyToClipboard(string html, string plainText = "")
            {
                var dataObject = CreateDataObject(html);
                Clipboard.SetDataObject(dataObject);
            }

            /// <summary>
            /// Generate HTML fragment data string with header that is required for the clipboard.
            /// </summary>
            /// <param name="html">the html to generate for</param>
            /// <returns>the resulted string</returns>
            private static string GetHtmlDataString(string html)
            {
                var sb = new StringBuilder();
                sb.AppendLine(Header);
                sb.AppendLine(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");

                // if given html already provided the fragments we won't add them
                int fragmentStart, fragmentEnd;
                int fragmentStartIdx = html.IndexOf(StartFragment, StringComparison.OrdinalIgnoreCase);
                int fragmentEndIdx = html.LastIndexOf(EndFragment, StringComparison.OrdinalIgnoreCase);

                // if html tag is missing add it surrounding the given html (critical)
                int htmlOpenIdx = html.IndexOf("<html", StringComparison.OrdinalIgnoreCase);
                int htmlOpenEndIdx = htmlOpenIdx > -1 ? html.IndexOf('>', htmlOpenIdx) + 1 : -1;
                int htmlCloseIdx = html.LastIndexOf("</html", StringComparison.OrdinalIgnoreCase);

                if (fragmentStartIdx < 0 && fragmentEndIdx < 0)
                {
                    int bodyOpenIdx = html.IndexOf("<body", StringComparison.OrdinalIgnoreCase);
                    int bodyOpenEndIdx = bodyOpenIdx > -1 ? html.IndexOf('>', bodyOpenIdx) + 1 : -1;

                    if (htmlOpenEndIdx < 0 && bodyOpenEndIdx < 0)
                    {
                        // the given html doesn't contain html or body tags so we need to add them and place start/end fragments around the given html only
                        sb.Append("<html><body>");
                        sb.Append(StartFragment);
                        fragmentStart = GetByteCount(sb);
                        sb.Append(html);
                        fragmentEnd = GetByteCount(sb);
                        sb.Append(EndFragment);
                        sb.Append("</body></html>");
                    }
                    else
                    {
                        // insert start/end fragments in the proper place (related to html/body tags if exists) so the paste will work correctly
                        int bodyCloseIdx = html.LastIndexOf("</body", StringComparison.OrdinalIgnoreCase);

                        if (htmlOpenEndIdx < 0)
                            sb.Append("<html>");
                        else
                            sb.Append(html, 0, htmlOpenEndIdx);

                        if (bodyOpenEndIdx > -1)
                            sb.Append(html, htmlOpenEndIdx > -1 ? htmlOpenEndIdx : 0,
                                bodyOpenEndIdx - (htmlOpenEndIdx > -1 ? htmlOpenEndIdx : 0));

                        sb.Append(StartFragment);
                        fragmentStart = GetByteCount(sb);

                        var innerHtmlStart = bodyOpenEndIdx > -1 ? bodyOpenEndIdx : (htmlOpenEndIdx > -1 ? htmlOpenEndIdx : 0);
                        var innerHtmlEnd = bodyCloseIdx > -1 ? bodyCloseIdx : (htmlCloseIdx > -1 ? htmlCloseIdx : html.Length);
                        sb.Append(html, innerHtmlStart, innerHtmlEnd - innerHtmlStart);

                        fragmentEnd = GetByteCount(sb);
                        sb.Append(EndFragment);

                        if (innerHtmlEnd < html.Length)
                            sb.Append(html, innerHtmlEnd, html.Length - innerHtmlEnd);

                        if (htmlCloseIdx < 0)
                            sb.Append("</html>");
                    }
                }
                else
                {
                    // handle html with existing start\end fragments just need to calculate the correct bytes offset (surround with html tag if missing)
                    if (htmlOpenEndIdx < 0)
                        sb.Append("<html>");
                    int start = GetByteCount(sb);
                    sb.Append(html);
                    fragmentStart = start + GetByteCount(sb, start, start + fragmentStartIdx) + StartFragment.Length;
                    fragmentEnd = start + GetByteCount(sb, start, start + fragmentEndIdx);
                    if (htmlCloseIdx < 0)
                        sb.Append("</html>");
                }

                // Back-patch offsets (scan only the header part for performance)
                sb.Replace("<<<<<<<<1", Header.Length.ToString("D9"), 0, Header.Length);
                sb.Replace("<<<<<<<<2", GetByteCount(sb).ToString("D9"), 0, Header.Length);
                sb.Replace("<<<<<<<<3", fragmentStart.ToString("D9"), 0, Header.Length);
                sb.Replace("<<<<<<<<4", fragmentEnd.ToString("D9"), 0, Header.Length);

                return sb.ToString();
            }

            /// <summary>
            /// Calculates the number of bytes produced by encoding the string in the string builder in UTF-8 and not .NET default string encoding.
            /// </summary>
            /// <param name="sb">the string builder to count its string</param>
            /// <param name="start">optional: the start index to calculate from (default - start of string)</param>
            /// <param name="end">optional: the end index to calculate to (default - end of string)</param>
            /// <returns>the number of bytes required to encode the string in UTF-8</returns>
            private static int GetByteCount(StringBuilder sb, int start = 0, int end = -1)
            {
                int count = 0;
                end = end > -1 ? end : sb.Length;
                for (int i = start; i < end; i++)
                {
                    _byteCount[0] = sb[i];
                    count += Encoding.UTF8.GetByteCount(_byteCount);
                }
                return count;
            }
        }

        public static Image ByteArrayToImage(byte[] source)
        {
            Image ret = null;
            using (MemoryStream ms = new MemoryStream(source)){
                ret = Image.FromStream(ms);
            };
            
            return ret;
        }

        private static Image[] getFrames(byte[] bytes)
        {
            MemoryStream ms = new MemoryStream(bytes);
            Image originalImg = Image.FromStream(ms, true, true);
            int numberOfFrames = originalImg.GetFrameCount(FrameDimension.Time);
            Image[] frames = new Image[numberOfFrames];

            for (int i = 0; i < numberOfFrames; i++)
            {
                originalImg.SelectActiveFrame(FrameDimension.Time, i);
                frames[i] = ((Image)originalImg.Clone());
            }

            return frames;
        }

        private static async Task ShortenVideo(string inputPath, string outputPath, double from, double newDuration)
        {
            var inputFile = new MediaFile { Filename = inputPath };
            var outputFile = new MediaFile { Filename = outputPath };

            using (var engine = new Engine())
            {
                engine.GetMetadata(inputFile);

                var options = new ConversionOptions();

                // This example will create a 25 second video, starting from the 
                // 30th second of the original video.
                //// First parameter requests the starting frame to cut the media from.
                //// Second parameter requests how long to cut the video.
                ///
                double inputFileDuration = inputFile.Metadata.Duration.TotalSeconds;
                newDuration = from + newDuration <= inputFileDuration ? newDuration : (from + newDuration) - inputFileDuration;

                options.CutMedia(TimeSpan.FromSeconds(from), TimeSpan.FromSeconds(newDuration));

                engine.Convert(inputFile, outputFile, options);
            }
            await Task.Yield();
        }
        private static async Task CreateGifFromMp4(string inputPath, string outputPath)
        {
            using (var engine = new Engine())
            {
                var inputFile = new MediaFile { Filename = inputPath };
                var outputFile = new MediaFile { Filename = outputPath };
                var options = new ConversionOptions { VideoFps = 5, VideoSize = VideoSize.Hd480};
                engine.Convert(inputFile, outputFile, options);
            }
            File.Delete(inputPath);
            await Task.Yield();
        }

        public static async Task<Video> UploadVideoAsync(string title, string desc, string inputPath)
        {
            UserCredential credential;
            using (var stream = new FileStream("cred/client_secret_256489060194-gsfeutp2lusklqv34dieal0uooim74bn.apps.googleusercontent.com.json", FileMode.Open, FileAccess.Read))
            {
                credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.FromStream(stream).Secrets,
                    // This OAuth 2.0 access scope allows an application to upload files to the
                    // authenticated user's YouTube channel, but doesn't allow other types of access.
                    new[] { YouTubeService.Scope.YoutubeUpload },
                    "user",
                    CancellationToken.None
                );
            }

            var youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
            });

            var video = new Video();
            video.Snippet = new VideoSnippet();
            video.Snippet.Title = title;
            video.Snippet.Description = desc;
            //video.Snippet.Tags = new string[] { "tag1", "tag2" };
            //video.Snippet.CategoryId = "22"; // See https://developers.google.com/youtube/v3/docs/videoCategories/list
            video.Status = new VideoStatus();
            video.Status.PrivacyStatus = "unlisted"; // or "private" or "public"
                                                     // Replace with path to actual movie file.

            using (var fileStream = new FileStream(inputPath, FileMode.Open))
            {
                var videosInsertRequest = youtubeService.Videos.Insert(video, "snippet,status", fileStream, "video/*");
                videosInsertRequest.ProgressChanged += videosInsertRequest_ProgressChanged;
                videosInsertRequest.ResponseReceived += videosInsertRequest_ResponseReceived;

                await videosInsertRequest.UploadAsync();
            }

            return video;
        }

        private static void videosInsertRequest_ProgressChanged(Google.Apis.Upload.IUploadProgress progress)
        {
            switch (progress.Status)
            {
                case UploadStatus.Uploading:
                    Console.WriteLine("{0} bytes sent.", progress.BytesSent);
                    break;

                case UploadStatus.Failed:
                    Console.WriteLine("An error prevented the upload from completing.\n{0}", progress.Exception);
                    break;
            }
        }

        private static void videosInsertRequest_ResponseReceived(Video video)
        {
            Console.WriteLine("Video id '{0}' was successfully uploaded.", video.Id);
        }

        private static string ClipBoardTemplate(string templatePath, string gifPath, string href, string linkTitle)
        {
            return File.ReadAllText(templatePath).Replace("{{##imagePath##}}", gifPath).Replace("{{##href##}}", href).Replace("{{##linkTitle##}}", linkTitle);
        }
        private static string GetVideoUrl(string initials, string videoId)
        {
            return "https://www.clicklearn.com/video/" + videoId + "/" + initials;
        }

        //https://inthetechpit.com/2019/01/31/resize-image-preserving-aspect-ratio-in-c/
        private static Image ResizeImage(Image imgToResize, Size size)
        {
            //Get the image current width  
            int sourceWidth = imgToResize.Width;
            //Get the image current height  
            int sourceHeight = imgToResize.Height;
            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;
            //Calulate  width with new desired size  
            nPercentW = ((float)size.Width / (float)sourceWidth);
            //Calculate height with new desired size  
            nPercentH = ((float)size.Height / (float)sourceHeight);
            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;
            //New Width  
            int destWidth = (int)(sourceWidth * nPercent);
            //New Height  
            int destHeight = (int)(sourceHeight * nPercent);
            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((System.Drawing.Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            // Draw image with new width and height  
            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();
            return (System.Drawing.Image)b;
        }
    }
}
