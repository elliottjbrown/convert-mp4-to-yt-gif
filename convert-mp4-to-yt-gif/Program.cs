﻿using System;
using System.IO;
using System.Threading.Tasks;
using MediaToolkit.Model;
using MediaToolkit.Options;
using MediaToolkit;
using Google.Apis.YouTube.v3.Data;
using Google.Apis.Upload;
using Google.Apis.Auth.OAuth2;
using Google.Apis.YouTube.v3;
using System.Threading;
using Google.Apis.Services;
using System.Reflection;
using System.Diagnostics;

namespace convert_mp4_to_yt_gif
{
    class Program
    {
        [STAThread]
        static async Task Main()
        {

            string dir = System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName).Split("\\bin")[0].Replace("\\", "/");
            if (dir.EndsWith("/")) dir = dir.Substring(0, dir.Length - 1);
            string filesDir = dir + "/files/";

            // input field
            string filePath = "GOLDENEYE.mp4";
            string title = "Elliott's test video";
            string desc = "This is a test upload to prove how the YoutubeApi works. There's a lot of interesting things going on here so there should be a lot of scope to introduce some excellent new features for our sales team.";

            // this doesn't load = I DON'T UNDERSTAND Async;
            Video ytVideo = await UploadVideoAsync(filePath, title, desc, dir, filesDir);

            double from = 100; // input field
            double duration = 5; // input field

            string newFilePath = filesDir + "shortened_" + filePath;

            await ShortenVideo(filesDir, filePath, newFilePath, from, duration);

            string convertedFilePath = Path.GetFileNameWithoutExtension(filePath);
            string sourcePath = newFilePath;
            string outputPath = filesDir + convertedFilePath + ".gif";

            await CreateGifFromMp4(sourcePath, outputPath);

            //copy gif to clipboard
            //using (var giffileStream = new FileStream(outputPath, FileMode.Open))
            //{
            //    // convert to stream to byteArray[]
            //    MemoryStream bgGif_ms = new MemoryStream();
            //    giffileStream.CopyTo(bgGif_ms);
            //    Clipboard.SetImage(giffileStream);
            //}
            // on creation of gif, convert gif back into stream. Copy to clipboard
            // ahref = www.clicklearn.com/video/ScTihwPpzLo/ejb
            // img = gifStream
        }

        private static async Task ShortenVideo(string dir, string filePath, string outputPath, double from, double newDuration)
        {
            var inputFile = new MediaFile { Filename = dir + filePath };
            var outputFile = new MediaFile { Filename = outputPath };

            using (var engine = new Engine())
            {
                engine.GetMetadata(inputFile);

                var options = new ConversionOptions();

                // This example will create a 25 second video, starting from the 
                // 30th second of the original video.
                //// First parameter requests the starting frame to cut the media from.
                //// Second parameter requests how long to cut the video.
                ///
                double inputFileDuration = inputFile.Metadata.Duration.TotalSeconds;
                newDuration = from + newDuration <= inputFileDuration ? newDuration : (from + newDuration) - inputFileDuration;

                options.CutMedia(TimeSpan.FromSeconds(from), TimeSpan.FromSeconds(newDuration));

                engine.Convert(inputFile, outputFile, options);
            }
            await Task.Yield();
        }
        private static async Task CreateGifFromMp4(string inputPath, string outputPath)
        {
            using (var engine = new Engine())
            {
                var inputFile = new MediaFile { Filename = inputPath };
                var outputFile = new MediaFile { Filename = outputPath };
                engine.Convert(inputFile, outputFile);
            }
            await Task.Yield();
        }

        public static async Task<Video> UploadVideoAsync(string filePath, string title, string desc, string dir, string fileDir)
        {
            UserCredential credential;
            using (var stream = new FileStream(dir + "/cred/client_secret_256489060194-gsfeutp2lusklqv34dieal0uooim74bn.apps.googleusercontent.com.json", FileMode.Open, FileAccess.Read))
            {
                credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.FromStream(stream).Secrets,
                    // This OAuth 2.0 access scope allows an application to upload files to the
                    // authenticated user's YouTube channel, but doesn't allow other types of access.
                    new[] { YouTubeService.Scope.YoutubeUpload },
                    "user",
                    CancellationToken.None
                );
            }

            var youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
            });

            var video = new Video();
            video.Snippet = new VideoSnippet();
            video.Snippet.Title = title;
            video.Snippet.Description = desc;
            //video.Snippet.Tags = new string[] { "tag1", "tag2" };
            //video.Snippet.CategoryId = "22"; // See https://developers.google.com/youtube/v3/docs/videoCategories/list
            video.Status = new VideoStatus();
            video.Status.PrivacyStatus = "unlisted"; // or "private" or "public"
                                                     // Replace with path to actual movie file.

            using (var fileStream = new FileStream(fileDir + filePath, FileMode.Open))
            {
                var videosInsertRequest = youtubeService.Videos.Insert(video, "snippet,status", fileStream, "video/*");
                videosInsertRequest.ProgressChanged += videosInsertRequest_ProgressChanged;
                videosInsertRequest.ResponseReceived += videosInsertRequest_ResponseReceived;

                await videosInsertRequest.UploadAsync();
            }

            return video;
        }

        private static void videosInsertRequest_ProgressChanged(Google.Apis.Upload.IUploadProgress progress)
        {
            switch (progress.Status)
            {
                case UploadStatus.Uploading:
                    Console.WriteLine("{0} bytes sent.", progress.BytesSent);
                    break;

                case UploadStatus.Failed:
                    Console.WriteLine("An error prevented the upload from completing.\n{0}", progress.Exception);
                    break;
            }
        }

        private static void videosInsertRequest_ResponseReceived(Video video)
        {
            Console.WriteLine("Video id '{0}' was successfully uploaded.", video.Id);
        }
    }
}
